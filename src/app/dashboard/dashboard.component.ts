import { Component, OnInit } from '@angular/core';

import { Library } from '../library';
import { LibraryService } from '../library.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  libraries: Library[] = [];

  constructor(private libraryService: LibraryService) { }

  ngOnInit() {
    this.getLibraries();
  }

  getLibraries(): void {
    this.libraryService.getLibraries()
      .subscribe(libraries => this.libraries = libraries);
  }

}
