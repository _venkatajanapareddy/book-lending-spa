import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Library } from '../library';
import { Book } from '../book';
import { LibraryService } from '../library.service';

@Component({
  selector: 'app-library-detail',
  templateUrl: './library-detail.component.html',
  styleUrls: ['./library-detail.component.css']
})
export class LibraryDetailComponent implements OnInit {

  library: Library;
  books: Book[] = [];
  private id;
  public isBookSelected = false;
  public selectedBookTitle;

  constructor(
    private route: ActivatedRoute,
    private libraryService: LibraryService,
  ) {
    this.id = +this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getLibrary();
    this.getBooks();
  }

  getLibrary(): void {

    this.libraryService.getLibrary(this.id)
      .subscribe(library => this.library = library);
  }

  getBooks(): void {
    this.libraryService.getLibraryBooks(this.id)
      .subscribe(books => this.books = books);
  }

  bookSelected(book) {
    this.selectedBookTitle = book.Title;
    this.isBookSelected = true;
  }

}
