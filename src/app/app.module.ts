import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LibraryService } from './library.service';
import { AppRoutingModule } from './/app-routing.module';
import { LibraryDetailComponent } from './library-detail/library-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LibraryDetailComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [LibraryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
