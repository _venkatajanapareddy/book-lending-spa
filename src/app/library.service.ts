import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Library } from './library';
import { Book } from './book';


@Injectable()
export class LibraryService {

  private getLibrariesUrl = 'https://libraryapi.azurewebsites.net/api/Libraries';
  private getLibrariesByIDUrl = 'https://libraryapi.azurewebsites.net/api/Libraries/';
  private getLibaryBooksUrl = 'https://libraryapi.azurewebsites.net/api/libraries/';

  getLibraries() {
    return this.http.get<Library[]>(this.getLibrariesUrl);
  }

  getLibrary(id) {
    return this.http.get<Library>(this.getLibrariesByIDUrl + id);
  }

  getLibraryBooks(id) {
    return this.http.get<Book[]>(this.getLibaryBooksUrl + id + '/librarybook');
  }

  constructor(private http: HttpClient) { }

}
